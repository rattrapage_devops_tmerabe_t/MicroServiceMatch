<?php
Doo::loadCore('db/DooModel');

class PhotoBase extends DooModel{

    /**
     * @var int Max length is 50.
     */
    public $id;

    /**
     * @var varchar Max length is 1000.
     */
    public $url;

    /**
     * @var varchar Max length is 20.
     */
    public $format;

    /**
     * @var int Max length is 50.
     */
    public $width;

    /**
     * @var int Max length is 50.
     */
    public $height;

    public $_table = 'photo';
    public $_primarykey = '';
    public $_fields = array('id','url','format','width','height');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'url' => array(
                        array( 'maxlength', 1000 ),
                        array( 'notnull' ),
                ),

                'format' => array(
                        array( 'maxlength', 20 ),
                        array( 'notnull' ),
                ),

                'width' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'height' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                )
            );
    }

}