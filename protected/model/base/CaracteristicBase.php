<?php
Doo::loadCore('db/DooModel');

class CaracteristicBase extends DooModel{

    /**
     * @var int Max length is 50.
     */
    public $user_id;

    /**
     * @var varchar Max length is 1000.
     */
    public $bio;

    /**
     * @var date
     */
    public $birth_date;

    /**
     * @var int Max length is 20.
     */
    public $gender;

    /**
     * @var int Max length is 50.
     */
    public $id_photo;

    public $_table = 'caracteristic';
    public $_primarykey = 'id_photo';
    public $_fields = array('user_id','bio','birth_date','gender','id_photo');

    public function getVRules() {
        return array(
                'user_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'bio' => array(
                        array( 'maxlength', 1000 ),
                        array( 'notnull' ),
                ),

                'birth_date' => array(
                        array( 'date' ),
                        array( 'notnull' ),
                ),

                'gender' => array(
                        array( 'integer' ),
                        array( 'maxlength', 20 ),
                        array( 'notnull' ),
                ),

                'id_photo' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                )
            );
    }

}