<?php
Doo::loadModel('Caracteristic');
Doo::loadController('BDDController');

class CaracteristicCtrl extends BDDController {

	public function GetCaracteristicByUserID() {		
		$u = new Caracteristic();
		$options = array(
				'select' => '*',
				'AsArray' => 'true',
				'where' => "user_id = '" . $this->params['user_id'] . "'"
		);
		$cara = $u->find($options);
		foreach ($cara as $one) {
			unset($one->_table);
			unset($one->_primarykey);
			unset($one->_fields);	
		}


		if (empty($cara)) {
			return $this->renderJSON(json_encode(null));
		}
	
		return $this->renderJSON(json_encode($cara));

	}

	public function AddCaracteristic() {

		$data = file_get_contents("php://input");
		$data = json_decode($data);

		$c = new Caracteristic($data);
		
			return $this->renderJSON(json_encode($c->insert()));
		}
	

	public function UpdateCaracteristic() {
		$data = file_get_contents("php://input");
		$d = json_decode($data);

		if ($d->user_id) {
			$c = new Caracteristic($d);
			var_dump($c);die;

			return $this->renderJSON(json_encode($c->update()));
		}
	}

}