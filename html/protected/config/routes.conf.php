<?php

$route['get']['/genModel'] = array(
		'BDDController',
		'genModel'
);

$route['get']['/genBDD'] = array(
		'BDDController',
		'genBDD'
);

/**
 * USERS
 */
// GET
$route['get']['/like/:user_id'] = array(
		'Ctrl/LikeCtrl',
		'GetAllLikeByUserId'
);

$route['get']['/likeMock/:user_id'] = array(
		'Ctrl/LikeCtrl',
		'likeMock'
);

$route['get']['/recommandation/:user_id/caracteristic'] = array(
		'Ctrl/CaracteristicCtrl',
		'GetCaracteristicByUserID'
);

$route['get']['/match'] = array(
		'Ctrl/MatchCtrl',
		'GetMyMatch'
);

$route['get']['/match/:user_id'] = array(
		'Ctrl/MatchCtrl',
		'GetMatchByUserId'
);

// POST
$route['post']['/like/:user_id'] = array(
		'Ctrl/LikeCtrl',
		'AddLike'
);

$route['post']['/recommandation/:user_id/caracteristic'] = array(
		'Ctrl/CaracteristicCtrl',
		'AddCaracteristic'
);

$route['post']['/match'] = array(
		'Ctrl/MatchCtrl',
		'AddMatch'
);

//PUT
$route['put']['/recommandation/:user_id/caracteristic'] = array(
		'Ctrl/CaracteristicCtrl',
		'UpdateCaracteristic'
);
?>