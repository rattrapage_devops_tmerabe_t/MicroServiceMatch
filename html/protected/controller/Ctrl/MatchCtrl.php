<?php
Doo::loadModel('Matchdb');
Doo::loadController('BDDController');

class MatchCtrl extends BDDController {

public function GetMyMatch()
{
	$currentUserId = 2;
	$l = new Matchdb();
		$options = array(
				'select' => '*',
				'AsArray' => 'true',
				'where' => "second_user_id = '" . $currentUserId ."'"
					);
		$match = $l->find($options);

		foreach ($match as $one) {
			unset($one->_table);
			unset($one->_primarykey);
			unset($one->_fields);	
			unset($one->byuser);	

		}


		if (empty($match)) {
			return $this->renderJSON(json_encode(null));
		}
		return $this->renderJSON(json_encode($match));
}

public function GetMatchByUserId()
{
	$l = new Matchdb();
		$options = array(
				'select' => '*',
				'AsArray' => 'true',
				'where' => "first_user_id = '" . $this->params['user_id'] . "'"
		);
		$match = $l->find($options);

		foreach ($match as $one) {
			unset($one->_table);
			unset($one->_primarykey);
			unset($one->_fields);	
			unset($one->byuser);	

		}


		if (empty($match)) {
			return $this->renderJSON(json_encode(null));
		}
		return $this->renderJSON(json_encode($match));
}
public function AddMatch()
{

		$data = file_get_contents("php://input");
		$data = json_decode($data);

		$newmatch = new Matchdb($data);
// var_dump(json_encode($newmatch));die;

		return $this->renderJSON(json_encode($newmatch->insert()));	
}




















































	public function getOneConversation() {

		$u = new Inbox();
		$options = array(
				'select' => '*',
				'AsArray' => 'true',
				'where' => "match_id = '" . $this->params['match_id'] . "'"
		);
		$inbox = $u->find($options);

		foreach ($inbox as $one) {
			unset($one->_table);
			unset($one->_primarykey);
			unset($one->_fields);	
		}


		if (empty($inbox)) {
			return $this->renderJSON(json_encode(null));
		}
		return $this->renderJSON(json_encode($inbox));
	}

	public function CreateOneConversation() {
		$data = file_get_contents("php://input");
		$data = json_decode($data);
		$u = new Inbox();

		$newInbox = new Inbox($data);
		$newInbox->match_id = $this->params['match_id'];
		return $this->renderJSON(json_encode($newInbox->insert()));
	}

	public function CreateOneMessage() {
		$data = file_get_contents("php://input");
		$data = json_decode($data);
		$u = new Inbox();

		$newInbox = new Inbox($data);
		return $this->renderJSON(json_encode($newInbox->insert()));
	}

	public function getAllConversation() {

		$i = new Inbox();
		$data = file_get_contents("php://input");
		$data = json_decode($data);

		$options = array(
				'select' => 'distinct match_id',
				'AsArray' => 'true'

		);

		$inbox = $i->find($options);

		foreach ($inbox as $one) {
		 	unset($one->_table);
			unset($one->_primarykey);
			unset($one->_fields);
			unset($one->id);	
			unset($one->from);	
			unset($one->to);
			unset($one->created_date);	
			unset($one->sent_date);	
			unset($one->content);	
		}
		return $this->renderJSON(json_encode($inbox));
	}

}