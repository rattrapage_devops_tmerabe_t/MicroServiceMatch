<?php
Doo::loadModel('Likedb');
Doo::loadController('BDDController');

class LikeCtrl extends BDDController {



public function Addlike()
{
		$currentUser = 10;
		$data = file_get_contents("php://input");
		$data = json_decode($data);
		$l = new Likedb();


		$newLike = new Likedb($data);
		$newLike->user_id = $this->params['user_id'];
		$newLike->status = 1;
		$newLike->byuser = $currentUser;

		return $this->renderJSON(json_encode($newLike->insert()));
}

public function likeMock() {
	$inbox = [['user_id' => '2', 'status' => '1'],['user_id' => '3', 'status' => '1']];
	return $this->renderJSON(json_encode($inbox));
}

public function GetAllLikeByUserId()
{
		$l = new Likedb();
		$options = array(
				'select' => '*',
				'AsArray' => 'true',
				'where' => "byuser = '" . $this->params['user_id'] . "'"
		);
		$like = $l->find($options);

		foreach ($like as $one) {
			unset($one->_table);
			unset($one->_primarykey);
			unset($one->_fields);	
			unset($one->byuser);	

		}


		if (empty($like)) {
			return $this->renderJSON(json_encode(null));
		}
		return $this->renderJSON(json_encode($like ));
}



}