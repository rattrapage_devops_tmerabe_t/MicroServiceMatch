<?php
Doo::loadCore('db/DooModel');

class MatchdbBase extends DooModel{

    /**
     * @var int Max length is 50.
     */
    public $match_id;

    /**
     * @var int Max length is 50.
     */
    public $first_user_id;

    /**
     * @var int Max length is 50.
     */
    public $second_user_id;

    /**
     * @var date
     */
    public $match_date;

    public $_table = 'matchdb';
    public $_primarykey = '';
    public $_fields = array('match_id','first_user_id','second_user_id','match_date');

    public function getVRules() {
        return array(
                'match_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'first_user_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'second_user_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'match_date' => array(
                        array( 'date' ),
                        array( 'optional' ),
                )
            );
    }

}