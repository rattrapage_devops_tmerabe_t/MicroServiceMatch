<?php
Doo::loadCore('db/DooModel');

class LikedbBase extends DooModel{

    /**
     * @var int Max length is 50.
     */
    public $user_id;

    /**
     * @var int Max length is 50.
     */
    public $status;

    /**
     * @var int Max length is 50.
     */
    public $byuser;

    public $_table = 'likedb';
    public $_primarykey = '';
    public $_fields = array('user_id','status','byuser');

    public function getVRules() {
        return array(
                'user_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'status' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'byuser' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                )
            );
    }

}