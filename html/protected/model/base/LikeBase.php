<?php
Doo::loadCore('db/DooModel');

class LikeBase extends DooModel{

    /**
     * @var int Max length is 50.
     */
    public $user_id;

    /**
     * @var int Max length is 50.
     */
    public $status;

    /**
     * @var int Max length is 50.
     */
    public $by;

    public $_table = 'like';
    public $_primarykey = '';
    public $_fields = array('user_id','status','by');

    public function getVRules() {
        return array(
                'user_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'status' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'by' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                )
            );
    }

}