<?php
Doo::loadCore('db/DooModel');

class UserBase extends DooModel {

    /**
     * @var bigint Max length is 20.  unsigned.
     */
    public $id_user;

    /**
     * @var varchar Max length is 200.
     */
    public $nom;

    /**
     * @var varchar Max length is 200.
     */
    public $prenom;

    /**
     * @var varchar Max length is 200.
     */
    public $email;
	
	/**
     * @var date.
     */
    public $dateDeNaissance;

    /**
     * @var varchar Max length is 200.
     */
    public $profession;

    /**
     * @var varchar Max length is 200.
     */
    public $salaire;
	
	/**
     * @var varchar Max length is 20.
     */
    public $typeDeContrat;
	
	/**
     * @var varchar Max length is 20.
     */
    public $garant;
	
	/**
     * @var varchar Max length is 20.
     */
    public $salaireGarant;

	/**
     * @var varchar Max length is 200.
     */
    public $motDePasse;
	
	/**
     * @var varchar Max length is 200.
     */
    public $lienPhoto;
	
	/**
     * @var varchar Max length is 20.
     */
    public $typeProfil;
    
	public $_table = 'Users';
    public $_primarykey = 'idUser';
    public $_fields = array('idUser', 
							'nom',
							'prenom',
							'email',
							'dateDeNaissance',
							'profession',
							'salaire',
							'typeDeContrat',
							'garant',
							'salaireGarant',
							'motDePasse',
							'lienPhoto',
							'typeProfil');
}